package ru.vartanyan.tm.exception.empty;

public class EmptyPasswordException extends Exception{

    public EmptyPasswordException() throws Exception {
        super("Error! Login cannot be null or empty...");
    }

}
