package ru.vartanyan.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractUserListener;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

@Component
public class UserLockByLoginListener extends AbstractUserListener {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "user-lock-by-login";
    }

    @Override
    public String description() {
        return "Lock user by login";
    }

    @Override
    @EventListener(condition = "@userLockByLoginListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final SessionDTO session = bootstrap.getSession();
        System.out.println("[LOCK USER BY LOGIN]");
        System.out.println("[ENTER LOGIN]");
        @NotNull final String login = TerminalUtil.nextLine();
        adminEndpoint.lockUserByLogin(login, session);
    }

}
