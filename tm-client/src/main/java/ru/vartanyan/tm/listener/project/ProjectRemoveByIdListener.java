package ru.vartanyan.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractProjectListener;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.util.TerminalUtil;

@Component
public class ProjectRemoveByIdListener extends AbstractProjectListener {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove project by Id";
    }

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        @Nullable final SessionDTO session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        System.out.println("[REMOVE PROJECT BY ID");
        System.out.println("[ENTER ID");
        @NotNull final String id = TerminalUtil.nextLine();
        projectEndpoint.removeProjectById(id, session);
        System.out.println("[PROJECT REMOVED]");
    }

}
