package ru.vartanyan.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.repository.ProjectRepository;

@Controller
public class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/project/create")
    public String create() {
        projectRepository.save(new Project());
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        projectRepository.removeById(id);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
        final Project project = projectRepository.findById(id);
        return new ModelAndView("project-edit", "project", project);
    }

    @PostMapping("/project/edit/{id}")
    public String edit(
            @ModelAttribute("project") @NotNull Project project, BindingResult result
    ) {
        projectRepository.save(project);
        return "redirect:/projects";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

}
